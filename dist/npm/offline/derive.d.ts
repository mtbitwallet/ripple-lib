import { deriveKeypair, deriveAddress } from 'ripple-keypairs';
export { deriveKeypair, deriveAddress };
//# sourceMappingURL=derive.d.ts.map