export declare type GeneratedAddress = {
    secret: string;
    address: string;
};
declare function generateAddressAPI(options?: any): GeneratedAddress;
export { generateAddressAPI };
//# sourceMappingURL=generate-address.d.ts.map