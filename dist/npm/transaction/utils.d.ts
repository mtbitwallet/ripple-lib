import * as common from '../common';
import { Memo, RippledAmount } from '../common/types/objects';
import { Instructions, Prepare } from './types';
import { RippleAPI } from '..';
export declare type ApiMemo = {
    MemoData?: string;
    MemoType?: string;
    MemoFormat?: string;
};
export declare type TransactionJSON = {
    Account: string;
    TransactionType: string;
    Memos?: {
        Memo: ApiMemo;
    }[];
    Flags?: number;
    Fulfillment?: string;
    [Field: string]: string | number | Array<any> | RippledAmount | undefined;
};
declare function prepareTransaction(txJSON: TransactionJSON, api: RippleAPI, instructions: Instructions): Promise<Prepare>;
declare function convertStringToHex(string: string): string;
declare function convertMemo(memo: Memo): {
    Memo: ApiMemo;
};
export { convertStringToHex, convertMemo, prepareTransaction, common };
//# sourceMappingURL=utils.d.ts.map