"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils = require("./utils");
const validate = utils.common.validate;
function createOrderCancellationTransaction(account, orderCancellation) {
    const txJSON = {
        TransactionType: 'OfferCancel',
        Account: account,
        OfferSequence: orderCancellation.orderSequence
    };
    if (orderCancellation.memos !== undefined) {
        txJSON.Memos = orderCancellation.memos.map(utils.convertMemo);
    }
    return txJSON;
}
function prepareOrderCancellation(address, orderCancellation, instructions = {}) {
    try {
        validate.prepareOrderCancellation({ address, orderCancellation, instructions });
        const txJSON = createOrderCancellationTransaction(address, orderCancellation);
        return utils.prepareTransaction(txJSON, this, instructions);
    }
    catch (e) {
        return Promise.reject(e);
    }
}
exports.default = prepareOrderCancellation;
//# sourceMappingURL=ordercancellation.js.map