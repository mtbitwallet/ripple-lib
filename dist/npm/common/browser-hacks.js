"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function setPrototypeOf(object, prototype) {
    Object.setPrototypeOf ? Object.setPrototypeOf(object, prototype) :
        object.__proto__ = prototype;
}
exports.setPrototypeOf = setPrototypeOf;
function getConstructorName(object) {
    if (!object.constructor.name) {
        return object.constructor.toString().match(/^function\s+([^(]*)/)[1];
    }
    return object.constructor.name;
}
exports.getConstructorName = getConstructorName;
//# sourceMappingURL=browser-hacks.js.map